provider "libvirt" {
    uri = "qemu:///system"
}

terraform {
 required_version = ">= 0.13"
  required_providers {
    libvirt = {
      source  = "dmacvicar/libvirt"
      version = "0.6.2"
    }
  }
}
    
resource "libvirt_cloudinit_disk" "oo-k8s-master01-cloudinit" {
    name = "oo-k8s-master01-cloudinit.iso"
    pool = "vms"
    user_data = data.template_file.user1_data.rendered
    network_config = data.template_file.network1_config.rendered
}
    
data "template_file" "user1_data" {
    template = file("${path.module}/cloudinit1.cfg")
}
    
data "template_file" "network1_config" {
    template = file("${path.module}/network1_config.cfg")
}

resource "libvirt_volume" "oo-k8s-master01-vda" {
    name = "oo-k8s-master01-vda.qcow2"
    pool = "vms"
    base_volume_name = "ubuntu-focal.img"
    base_volume_pool = "isos"
    size = "53687091200"
    format = "qcow2"
}

resource "libvirt_domain" "oo-k8s-master01" {
    name = "oo-k8s-master01"
    memory = "8192"
    vcpu = "4"
    machine = "pc-i440fx-rhel7.6.0"

    cpu = {
           mode = "host-passthrough"
    }

    cloudinit = libvirt_cloudinit_disk.oo-k8s-master01-cloudinit.id

    console {
        type        = "pty"
        target_port = "0"
        target_type = "serial"
    }

    console {
        type        = "pty"
        target_port = "1"
        target_type = "virtio"
    }

    network_interface {
        network_name = "net-10.48.10"
        addresses = ["10.48.10.5"]
    }

    disk {
        volume_id = libvirt_volume.oo-k8s-master01-vda.id
    }

    video {
        type = "vga"
    }
    
    graphics {
        type = "vnc"
        listen_type = "address"
        autoport = true
    }
}

resource "libvirt_cloudinit_disk" "oo-k8s-master02-cloudinit" {
    name = "oo-k8s-master02-cloudinit.iso"
    pool = "vms"
    user_data = data.template_file.user2_data.rendered
    network_config = data.template_file.network2_config.rendered
}
    
data "template_file" "user2_data" {
    template = file("${path.module}/cloudinit2.cfg")
}
    
data "template_file" "network2_config" {
    template = file("${path.module}/network2_config.cfg")
}

resource "libvirt_volume" "oo-k8s-master02-vda" {
    name = "oo-k8s-master02-vda.qcow2"
    pool = "vms"
    base_volume_name = "ubuntu-focal.img"
    base_volume_pool = "isos"
    size = "53687091200"
    format = "qcow2"
}

resource "libvirt_domain" "oo-k8s-master02" {
    name = "oo-k8s-master02"
    memory = "8192"
    vcpu = "4"
    machine = "pc-i440fx-rhel7.6.0"

    cpu = {
           mode = "host-passthrough"
    }

    cloudinit = libvirt_cloudinit_disk.oo-k8s-master02-cloudinit.id

    console {
        type        = "pty"
        target_port = "0"
        target_type = "serial"
    }

    console {
        type        = "pty"
        target_port = "1"
        target_type = "virtio"
    }

    network_interface {
        network_name = "net-10.48.10"
        addresses = ["10.48.10.6"]
    }

    disk {
        volume_id = libvirt_volume.oo-k8s-master02-vda.id
    }

    video {
        type = "vga"
    }
    
    graphics {
        type = "vnc"
        listen_type = "address"
        autoport = true
    }
}

resource "libvirt_cloudinit_disk" "oo-k8s-master03-cloudinit" {
    name = "oo-k8s-master03-cloudinit.iso"
    pool = "vms"
    user_data = data.template_file.user3_data.rendered
    network_config = data.template_file.network3_config.rendered
}
    
data "template_file" "user3_data" {
    template = file("${path.module}/cloudinit3.cfg")
}
    
data "template_file" "network3_config" {
    template = file("${path.module}/network3_config.cfg")
}

resource "libvirt_volume" "oo-k8s-master03-vda" {
    name = "oo-k8s-master03-vda.qcow2"
    pool = "vms"
    base_volume_name = "ubuntu-focal.img"
    base_volume_pool = "isos"
    size = "53687091200"
    format = "qcow2"
}

resource "libvirt_domain" "oo-k8s-master03" {
    name = "oo-k8s-master03"
    memory = "8192"
    vcpu = "4"
    machine = "pc-i440fx-rhel7.6.0"

    cpu = {
           mode = "host-passthrough"
    }

    cloudinit = libvirt_cloudinit_disk.oo-k8s-master03-cloudinit.id

    console {
        type        = "pty"
        target_port = "0"
        target_type = "serial"
    }

    console {
        type        = "pty"
        target_port = "1"
        target_type = "virtio"
    }

    network_interface {
        network_name = "net-10.48.10"
        addresses = ["10.48.10.7"]
    }

    disk {
        volume_id = libvirt_volume.oo-k8s-master03-vda.id
    }

    video {
        type = "vga"
    }
    
    graphics {
        type = "vnc"
        listen_type = "address"
        autoport = true
    }
}

resource "libvirt_cloudinit_disk" "oo-k8s-worker01-cloudinit" {
    name = "oo-k8s-worker01-cloudinit.iso"
    pool = "vms"
    user_data = data.template_file.user4_data.rendered
    network_config = data.template_file.network4_config.rendered
}
    
data "template_file" "user4_data" {
    template = file("${path.module}/cloudinit4.cfg")
}
    
data "template_file" "network4_config" {
    template = file("${path.module}/network4_config.cfg")
}

resource "libvirt_volume" "oo-k8s-worker01-vda" {
    name = "oo-k8s-worker01-vda.qcow2"
    pool = "vms"
    base_volume_name = "ubuntu-focal.img"
    base_volume_pool = "isos"
    size = "53687091200"
    format = "qcow2"
}

resource "libvirt_domain" "oo-k8s-worker01" {
    name = "oo-k8s-worker01"
    memory = "4096"
    vcpu = "2"
    machine = "pc-i440fx-rhel7.6.0"

    cpu = {
           mode = "host-passthrough"
    }

    cloudinit = libvirt_cloudinit_disk.oo-k8s-worker01-cloudinit.id

    console {
        type        = "pty"
        target_port = "0"
        target_type = "serial"
    }

    console {
        type        = "pty"
        target_port = "1"
        target_type = "virtio"
    }

    network_interface {
        network_name = "net-10.48.10"
        addresses = ["10.48.10.8"]
    }

    disk {
        volume_id = libvirt_volume.oo-k8s-worker01-vda.id
    }

    video {
        type = "vga"
    }
    
    graphics {
        type = "vnc"
        listen_type = "address"
        autoport = true
    }
}

resource "libvirt_cloudinit_disk" "oo-k8s-worker02-cloudinit" {
    name = "oo-k8s-worker02-cloudinit.iso"
    pool = "vms"
    user_data = data.template_file.user5_data.rendered
    network_config = data.template_file.network5_config.rendered
}
    
data "template_file" "user5_data" {
    template = file("${path.module}/cloudinit5.cfg")
}
    
data "template_file" "network5_config" {
    template = file("${path.module}/network5_config.cfg")
}

resource "libvirt_volume" "oo-k8s-worker02-vda" {
    name = "oo-k8s-worker02-vda.qcow2"
    pool = "vms"
    base_volume_name = "ubuntu-focal.img"
    base_volume_pool = "isos"
    size = "53687091200"
    format = "qcow2"
}

resource "libvirt_domain" "oo-k8s-worker02" {
    name = "oo-k8s-worker02"
    memory = "4096"
    vcpu = "2"
    machine = "pc-i440fx-rhel7.6.0"

    cpu = {
           mode = "host-passthrough"
    }

    cloudinit = libvirt_cloudinit_disk.oo-k8s-worker02-cloudinit.id

    console {
        type        = "pty"
        target_port = "0"
        target_type = "serial"
    }

    console {
        type        = "pty"
        target_port = "1"
        target_type = "virtio"
    }

    network_interface {
        network_name = "net-10.48.10"
        addresses = ["10.48.10.9"]
    }

    disk {
        volume_id = libvirt_volume.oo-k8s-worker02-vda.id
    }

    video {
        type = "vga"
    }
    
    graphics {
        type = "vnc"
        listen_type = "address"
        autoport = true
    }
}

resource "libvirt_cloudinit_disk" "oo-k8s-lb-01-cloudinit" {
    name = "oo-k8s-lb-01-cloudinit.iso"
    pool = "vms"
    user_data = data.template_file.user6_data.rendered
    network_config = data.template_file.network6_config.rendered
}
    
data "template_file" "user6_data" {
    template = file("${path.module}/cloudinit6.cfg")
}
    
data "template_file" "network6_config" {
    template = file("${path.module}/network6_config.cfg")
}

resource "libvirt_volume" "oo-k8s-lb-01-vda" {
    name = "oo-k8s-lb-01-vda.qcow2"
    pool = "vms"
    base_volume_name = "ubuntu-focal.img"
    base_volume_pool = "isos"
    size = "53687091200"
    format = "qcow2"
}

resource "libvirt_domain" "oo-k8s-lb-01" {
    name = "oo-k8s-lb-01"
    memory = "4096"
    vcpu = "2"
    machine = "pc-i440fx-rhel7.6.0"

    cpu = {
           mode = "host-passthrough"
    }

    cloudinit = libvirt_cloudinit_disk.oo-k8s-lb-01-cloudinit.id

    console {
        type        = "pty"
        target_port = "0"
        target_type = "serial"
    }

    console {
        type        = "pty"
        target_port = "1"
        target_type = "virtio"
    }

    network_interface {
        network_name = "net-10.48.10"
        addresses = ["10.48.10.10"]
    }

    disk {
        volume_id = libvirt_volume.oo-k8s-lb-01-vda.id
    }

    video {
        type = "vga"
    }
    
    graphics {
        type = "vnc"
        listen_type = "address"
        autoport = true
    }
}

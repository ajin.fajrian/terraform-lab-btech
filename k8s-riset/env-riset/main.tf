provider "libvirt" {
    uri = "qemu:///system"
}

terraform {
 required_version = ">= 0.13"
  required_providers {
    libvirt = {
      source  = "dmacvicar/libvirt"
      version = "0.6.2"
    }
  }
}
    
resource "libvirt_cloudinit_disk" "risetkube-master-01-cloudinit" {
    name = "risetkube-master-01-cloudinit.iso"
    pool = "vms"
    user_data = data.template_file.user1_data.rendered
    network_config = data.template_file.network1_config.rendered
}
    
data "template_file" "user1_data" {
    template = file("${path.module}/cloudinit1.cfg")
}
    
data "template_file" "network1_config" {
    template = file("${path.module}/network1_config.cfg")
}

resource "libvirt_volume" "risetkube-master-01-vda" {
    name = "risetkube-master-01-vda.qcow2"
    pool = "vms"
    base_volume_name = "ubuntu-jammy.img"
    base_volume_pool = "isos"
    size = "107374182400"
    format = "qcow2"
}

resource "libvirt_domain" "risetkube-master-01" {
    name = "risetkube-master-01"
    memory = "8192"
    vcpu = "4"
    machine = "pc-i440fx-rhel7.6.0"

    cpu = {
           mode = "host-passthrough"
    }

    cloudinit = libvirt_cloudinit_disk.risetkube-master-01-cloudinit.id

    console {
        type        = "pty"
        target_port = "0"
        target_type = "serial"
    }

    console {
        type        = "pty"
        target_port = "1"
        target_type = "virtio"
    }

    network_interface {
        network_name = "net-10.48.10"
        addresses = ["10.48.10.21"]
    }

    disk {
        volume_id = libvirt_volume.risetkube-master-01-vda.id
    }

    video {
        type = "vga"
    }
    
    graphics {
        type = "vnc"
        listen_type = "address"
        autoport = true
    }
}

resource "libvirt_cloudinit_disk" "risetkube-master-02-cloudinit" {
    name = "risetkube-master-02-cloudinit.iso"
    pool = "vms"
    user_data = data.template_file.user2_data.rendered
    network_config = data.template_file.network2_config.rendered
}
    
data "template_file" "user2_data" {
    template = file("${path.module}/cloudinit2.cfg")
}
    
data "template_file" "network2_config" {
    template = file("${path.module}/network2_config.cfg")
}

resource "libvirt_volume" "risetkube-master-02-vda" {
    name = "risetkube-master-02-vda.qcow2"
    pool = "vms"
    base_volume_name = "ubuntu-jammy.img"
    base_volume_pool = "isos"
    size = "107374182400"
    format = "qcow2"
}

resource "libvirt_domain" "risetkube-master-02" {
    name = "risetkube-master-02"
    memory = "8192"
    vcpu = "4"
    machine = "pc-i440fx-rhel7.6.0"

    cpu = {
           mode = "host-passthrough"
    }

    cloudinit = libvirt_cloudinit_disk.risetkube-master-02-cloudinit.id

    console {
        type        = "pty"
        target_port = "0"
        target_type = "serial"
    }

    console {
        type        = "pty"
        target_port = "1"
        target_type = "virtio"
    }

    network_interface {
        network_name = "net-10.48.10"
        addresses = ["10.48.10.22"]
    }

    disk {
        volume_id = libvirt_volume.risetkube-master-02-vda.id
    }

    video {
        type = "vga"
    }
    
    graphics {
        type = "vnc"
        listen_type = "address"
        autoport = true
    }
}

resource "libvirt_cloudinit_disk" "risetkube-master-03-cloudinit" {
    name = "risetkube-master-03-cloudinit.iso"
    pool = "vms"
    user_data = data.template_file.user3_data.rendered
    network_config = data.template_file.network3_config.rendered
}
    
data "template_file" "user3_data" {
    template = file("${path.module}/cloudinit3.cfg")
}
    
data "template_file" "network3_config" {
    template = file("${path.module}/network3_config.cfg")
}

resource "libvirt_volume" "risetkube-master-03-vda" {
    name = "risetkube-master-03-vda.qcow2"
    pool = "vms"
    base_volume_name = "ubuntu-jammy.img"
    base_volume_pool = "isos"
    size = "107374182400"
    format = "qcow2"
}

resource "libvirt_domain" "risetkube-master-03" {
    name = "risetkube-master-03"
    memory = "8192"
    vcpu = "4"
    machine = "pc-i440fx-rhel7.6.0"

    cpu = {
           mode = "host-passthrough"
    }

    cloudinit = libvirt_cloudinit_disk.risetkube-master-03-cloudinit.id

    console {
        type        = "pty"
        target_port = "0"
        target_type = "serial"
    }

    console {
        type        = "pty"
        target_port = "1"
        target_type = "virtio"
    }

    network_interface {
        network_name = "net-10.48.10"
        addresses = ["10.48.10.23"]
    }

    disk {
        volume_id = libvirt_volume.risetkube-master-03-vda.id
    }

    video {
        type = "vga"
    }
    
    graphics {
        type = "vnc"
        listen_type = "address"
        autoport = true
    }
}

resource "libvirt_cloudinit_disk" "risetkube-worker-01-cloudinit" {
    name = "risetkube-worker-01-cloudinit.iso"
    pool = "vms"
    user_data = data.template_file.user4_data.rendered
    network_config = data.template_file.network4_config.rendered
}
    
data "template_file" "user4_data" {
    template = file("${path.module}/cloudinit4.cfg")
}
    
data "template_file" "network4_config" {
    template = file("${path.module}/network4_config.cfg")
}

resource "libvirt_volume" "risetkube-worker-01-vda" {
    name = "risetkube-worker-01-vda.qcow2"
    pool = "vms"
    base_volume_name = "ubuntu-jammy.img"
    base_volume_pool = "isos"
    size = "107374182400"
    format = "qcow2"
}

resource "libvirt_domain" "risetkube-worker-01" {
    name = "risetkube-worker-01"
    memory = "12288"
    vcpu = "4"
    machine = "pc-i440fx-rhel7.6.0"

    cpu = {
           mode = "host-passthrough"
    }

    cloudinit = libvirt_cloudinit_disk.risetkube-worker-01-cloudinit.id

    console {
        type        = "pty"
        target_port = "0"
        target_type = "serial"
    }

    console {
        type        = "pty"
        target_port = "1"
        target_type = "virtio"
    }

    network_interface {
        network_name = "net-10.48.10"
        addresses = ["10.48.10.31"]
    }

    disk {
        volume_id = libvirt_volume.risetkube-worker-01-vda.id
    }

    video {
        type = "vga"
    }
    
    graphics {
        type = "vnc"
        listen_type = "address"
        autoport = true
    }
}

resource "libvirt_cloudinit_disk" "risetkube-worker-02-cloudinit" {
    name = "risetkube-worker-02-cloudinit.iso"
    pool = "vms"
    user_data = data.template_file.user5_data.rendered
    network_config = data.template_file.network5_config.rendered
}
    
data "template_file" "user5_data" {
    template = file("${path.module}/cloudinit5.cfg")
}
    
data "template_file" "network5_config" {
    template = file("${path.module}/network5_config.cfg")
}

resource "libvirt_volume" "risetkube-worker-02-vda" {
    name = "risetkube-worker-02-vda.qcow2"
    pool = "vms"
    base_volume_name = "ubuntu-jammy.img"
    base_volume_pool = "isos"
    size = "107374182400"
    format = "qcow2"
}

resource "libvirt_domain" "risetkube-worker-02" {
    name = "risetkube-worker-02"
    memory = "12288"
    vcpu = "4"
    machine = "pc-i440fx-rhel7.6.0"

    cpu = {
           mode = "host-passthrough"
    }

    cloudinit = libvirt_cloudinit_disk.risetkube-worker-02-cloudinit.id

    console {
        type        = "pty"
        target_port = "0"
        target_type = "serial"
    }

    console {
        type        = "pty"
        target_port = "1"
        target_type = "virtio"
    }

    network_interface {
        network_name = "net-10.48.10"
        addresses = ["10.48.10.32"]
    }

    disk {
        volume_id = libvirt_volume.risetkube-worker-02-vda.id
    }

    video {
        type = "vga"
    }
    
    graphics {
        type = "vnc"
        listen_type = "address"
        autoport = true
    }
}
